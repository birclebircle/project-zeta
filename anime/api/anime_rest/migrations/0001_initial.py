# Generated by Django 4.0.3 on 2023-10-14 17:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Anime',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('watched', models.BooleanField(default=False)),
                ('year', models.SmallIntegerField(max_length=4)),
                ('episodes', models.SmallIntegerField(max_length=5)),
                ('genre', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='anime', to='anime_rest.genre')),
            ],
        ),
    ]
