from django.urls import path
from .views import (
    api_list_anime,
)

urlpatterns = [
    path("anime/", api_list_anime, name="api_list_anime"),
]
