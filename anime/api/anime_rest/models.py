from django.db import models


class Genre(models.Model):
    name = models.CharField(max_length=50)


class Anime(models.Model):
    title = models.CharField(max_length=50)
    watched = models.BooleanField(default=False)
    year = models.SmallIntegerField()
    episodes = models.SmallIntegerField()
    genre = models.ForeignKey(
        Genre,
        related_name="anime",
        on_delete=models.CASCADE,
    )


# Create your models here.
