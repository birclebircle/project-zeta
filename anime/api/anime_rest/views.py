from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Anime, Genre
from common.json import ModelEncoder


class AnimeEncoder(ModelEncoder):
    model = Anime
    properties = [
        "id",
        "title",
        "watched",
        "year",
        "episodes",
        "genre",
    ]


class GenreEncoder(ModelEncoder):
    model = Genre
    properties = [
        "id",
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_anime(request):
    if request.method == "GET":
        anime = Anime.objects.all()
        return JsonResponse(
            {"anime": anime},
            encoder=AnimeEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            anime = Anime.objects.create(**content)
            return JsonResponse(
                anime,
                encoder=AnimeEncoder,
                safe=False,
            )
        except Anime.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not Create Anime"}
            )
            response.status_code = 400
            return response


# Create your views here.
